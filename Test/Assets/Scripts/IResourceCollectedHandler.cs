﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Implement to receive messages relating to resources being collected.
/// </summary>
public interface IResourceCollectedHandler : IEventSystemHandler
{ 
    /// <summary>
    /// Called when a resource is collected.
    /// </summary>
    /// <param name="resource">The resource that was collected</param>
    void OnResourceCollected(TappableResource2 resource);	

}
