﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TappableResource2 : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        // Trigger the event that the resource was collected.
        EventManager2.TriggerEvent<IResourceCollectedHandler>((x, y) => x.OnResourceCollected(this));
    }
}
