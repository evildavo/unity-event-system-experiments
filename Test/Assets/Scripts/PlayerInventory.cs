﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInventory : MonoBehaviour 
{
    private UnityAction<object> onResourceCollected;

    private void Awake()
    {
        onResourceCollected = new UnityAction<object>(OnResourceCollected);
    }

    private void OnEnable()
    {
        EventManager.StartListening("resource collected", onResourceCollected);
    }

    private void OnDisable()
    {
        EventManager.StopListening("resource collected", onResourceCollected);
    }

    private void OnResourceCollected(object argument)
    {
        TappableResource tappable = argument as TappableResource;

        Debug.Log("Resource collected: " + tappable.name);
    }

}
