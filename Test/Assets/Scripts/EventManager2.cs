﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Service for managing event messaging between scripts.
/// </summary>
public class EventManager2 : MonoBehaviour
{
    private HashSet<GameObject> listeners;

    private static EventManager2 instance;

    
    public static void StartListening(GameObject listener)
    {
        Instance.listeners.Add(listener);
    }

    public static void StopListening(GameObject listener)
    {
        // If the instance is already removed return.
        if (instance == null) return;

        Instance.listeners.Remove(listener);
    }

    public static void TriggerEvent<T>(ExecuteEvents.EventFunction<T> functor) where T : IEventSystemHandler
    {
        foreach (GameObject ob in Instance.listeners)
        {
            if (ExecuteEvents.CanHandleEvent<T>(ob))
            {
                ExecuteEvents.Execute(ob, null, functor);
            }
        }
    }
    


    // Returns a reference to the EventManager, locating it if it we don't have one.
    private static EventManager2 Instance
    {
        get
        {
            // If we don't have a reference to the EventManager instance, find one in the scene.
            if (!instance)
            {
                instance = FindObjectOfType<EventManager2>();

                if (!instance)
                {
                    Debug.LogError("There needs to be one active EventManager script on a GameObject in the scene.");
                }
                else
                {
                    // Initialise the instance for the first time.
                    instance.Initialise();
                }
            }
            return instance;
        }
    }

    void Initialise()
    {
        // Create the listener list if it hasn't been created already.
        if (listeners == null)
        {
            listeners = new HashSet<GameObject>();
        }
    }


}

