﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Service for managing event messaging between scripts.
/// </summary>
public class EventManager : MonoBehaviour 
{
    // A UnityEvent that accepts one parameter of type object.
    [System.Serializable]
    public class ObjectEvent : UnityEvent<object> {}

    // Stores our events.
    private Dictionary<string, ObjectEvent> eventDictionary;

    private static EventManager instance;
	
    /// <summary>
    /// Returns a reference to the EventManager, locating it if it we don't have one.
    /// </summary>
    public static EventManager Instance
    {
        get
        {
            // If we don't have a reference to the EventManager instance, find one in the scene.
            if (!instance)
            {
                instance = FindObjectOfType<EventManager>();

                if (!instance)
                {
                    Debug.LogError("There needs to be one active EventManager script on a GameObject in the scene.");
                }
                else
                {
                    // Initialise the instance for the first time.
                    instance.Initialise();
                }
            }
            return instance;
        }
    }

    /// <summary>
    /// Register the given event listener to receive events of type eventName.
    /// </summary>
    /// <param name="eventName">Name of the event to listen for.</param>
    /// <param name="listener">Function to invoke when the event occurs.</param>
    public static void StartListening(string eventName, UnityAction<object> listener)
    {
        // Attempt to locate an event with this name in the eventDictionary.
        ObjectEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            // We found an event with this name, so add our listener to it.
            thisEvent.AddListener(listener);
        }
        else
        {
            // If no event exists with this name create it and add it to the eventDictionary.
            thisEvent = new ObjectEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    /// <summary>
    /// Stop receiving events of type eventName for the given event listener.
    /// </summary>
    /// <param name="eventName">Name of the event that we're removing the listener from.</param>
    /// <param name="listener">Listener to remove.</param>
    public static void StopListening(string eventName, UnityAction<object> listener)
    {
        // If the instance is already removed return.
        if (instance == null) return;

        // If we can locate an event with this name in the eventDictionary remove this listener from it.
        ObjectEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    /// <summary>
    /// Triggers the event of type eventName, notifying all listeners.
    /// </summary>
    /// <remarks>Allows a custom argument to be passed along.</remarks>
    /// <param name="eventName">The name of the event to trigger.</param>
    /// <param name="argument">Custom data to pass along.</param>
    public static void TriggerEvent(string eventName, object argument)
    {
        ObjectEvent thisEvent = null;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(argument);
        }
    }

    void Initialise()
    {
        // Create the dictionary if it hasn't been created already.
        if (eventDictionary == null)
        {
            eventDictionary = new Dictionary<string, ObjectEvent>();
        }
    }

}

