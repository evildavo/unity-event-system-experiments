﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TappableResource : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        // Trigger the event that the resource was collected.
        EventManager.TriggerEvent("resource collected", this);
    }
}
