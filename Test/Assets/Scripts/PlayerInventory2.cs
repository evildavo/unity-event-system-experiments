﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInventory2 : MonoBehaviour, IResourceCollectedHandler
{

    private void OnEnable()
    {
        EventManager2.StartListening(gameObject);
    }

    private void OnDisable()
    {
        EventManager2.StopListening(gameObject);
    }

    public void OnResourceCollected(TappableResource2 resource)
    {
        Debug.Log("Resource collected: " + resource.name);
    }

}
